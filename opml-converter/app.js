const htmlparser = require('htmlparser2');
const axios = require('axios');
const fs = require('fs');
const CONFIG = require('./config.json');
var token = "";

const parser = new htmlparser.Parser({
    onopentag: function(name, attribs){
        if(name === "outline" && attribs.type === "rss"){
            console.log(attribs.xmlurl + " " + attribs.title);
            axios({
                method: 'POST',
                url: `${CONFIG.API_ENDPOINT}/feed?url=${attribs.xmlurl}&name=${attribs.title}`,
                headers: {'x-access-token': token}
            });
        }
    }
}, {decodeEntities: true});

axios.post(CONFIG.API_ENDPOINT + '/login', {
    username: CONFIG.userAccount,
    password: CONFIG.userPassword
})
.then((response) => {
    console.log('Auth');
    token = response.data.token;
    fs.readFile(CONFIG.filePath, 'utf8', function(err, data) {
        if (err) {
            return console.log(err);
        } else {
            parser.write(data);
        }
    });
});
